from django.contrib import admin

# Register your models here.
from apps.bot.models import TelegramUsers

admin.site.register(TelegramUsers)