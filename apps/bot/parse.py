from django.http import HttpResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response

from apps.bot.views import post_message_to_telegram


def parser_aqicn():
    import requests
    from bs4 import BeautifulSoup

    link = 'https://aqicn.org/city/uzbekistan/tashkent/us-embassy/'
    response = requests.get(link).text
    soup = BeautifulSoup(response, 'lxml')
    div_maincityview = soup.find('div', id='maincityview')
    div_aqiwgtvalue = div_maincityview.find('div', id='aqiwgtvalue')
    post_message_to_telegram(div_aqiwgtvalue.text)
    print(div_aqiwgtvalue.text)


@api_view(['GET'])
def parser_cbu_view(request):
    import requests
    from bs4 import BeautifulSoup

    link = 'https://aqicn.org/city/uzbekistan/tashkent/us-embassy/'
    response = requests.get(link).text
    soup = BeautifulSoup(response, 'lxml')
    div_maincityview = soup.find('div', id='maincityview')
    div_aqiwgtvalue = div_maincityview.find('div', id='aqiwgtvalue')

    print(div_aqiwgtvalue.text)
    res = {"msg", post_message_to_telegram(div_aqiwgtvalue.text)}
    return Response(res)
